# Tip calculator

Lightweight tip calculator written in pure Javascript. No any frameworks!   
* Application's tests are written with [NodeJs v5.10.0](https://nodejs.org/en/), so to be sure everything will work you need at least 4+ version.

Bon Appetit!

## Links
[Demo](http://js-tip-calculator.donskikh.com)   
[Source Code](https://bitbucket.org/donskikh/js-tip-calculator)   
[Unit tests](http://js-tip-calculator.donskikh.com/unit-tests)   
[Documentation](http://js-tip-calculator.donskikh.com/documentation)   
[Portfolio](http://www.donskikh.com)

## Run
Just run two commands in shell:   
```npm install```   
and   
```gulp serve```   
It will run local server accessible at
http://localhost:8080 with live-reload ability.

## Develop
All source code available at the ```./src``` directory.

### Get production-ready version
```gulp build --production```   
The result will be placed in the ```./build``` directory.

### Unit tests
Are written with [Jasmine](http://jasmine.github.io) and work with [Karma](https://karma-runner.github.io/0.13/index.html) test-runner.   
Placed at the ```./spec``` directory.
You can easily run them with:   
```gulp test```   

### Acceptance tests
Are written with amazing [CodeceptJS](http://codecept.io) framework.   
Placed at the ```./acceptance``` directory.    
Configuration file: ```codecept.json```

To run them you need to follow few steps:   

1) [Donwload](http://docs.seleniumhq.org/download/) and run selenium-server:   
```java -jar selenium-server-standalone-x.xx.xxx.jar```   
2) Run application:   
```gulp serve```   
3) Run acceptance tests:   
```codeceptjs run --steps```

### Gulp
* directory: ```./gulpTasks``` 

The next gulp tasks are available in the application:

```gulp build``` (prepare for develop|production. Output: ./build)   
```gulp clean``` (remove the ./build directory)   
```gulp default``` (run build)   
```gulp html``` (minify and inject scripts/styles)   
```gulp images``` (optimize images)   
```gulp scripts``` (minify, concatenate and uglify for production)   
```gulp serve``` (run local server with live-reload)   
```gulp styles``` (minify, concatenate and uglify for production)   
```gulp test``` (run unit test)   
```gulp watch``` (watch on html, css, js files and the build directory)   
```gulp documentation``` (create documentation for the project: ./html-documentation)

* Every task could be run with the ```--production``` flag.
* Unit tests could be run with the ```--debug``` flag. It will run them with Chrome instead of PhantomJs, and switch Karma's ```singleRun``` parameter to false to allow you to debug everything within browser's console.


## Licence
The MIT License (MIT) Copyright (c) 2016 Donskikh A.V.
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.