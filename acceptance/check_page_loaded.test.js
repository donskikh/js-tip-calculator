Feature('I can get the page with an application scenario');

Before((I) => {
    I.amOnPage('/');
});

Scenario('Home page with appllication has been loaded', (I) => {
    I.see('Tip Calculator');
});

Scenario('I can see required fileds and button', (I) => {
    I.seeElement('#cost');
    I.seeElement('#serviceQuality');
    I.seeElement('#amountOfPeople');
    I.seeElement('#actionCall');
});