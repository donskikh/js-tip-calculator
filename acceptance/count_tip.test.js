Feature('I can count tip scenario');

Before((I) => {
    I.amOnPage('/');
    I.clearField('#cost');
    I.clearField('#amountOfPeople');
});

Scenario('I can count a tip for my own after a good dinner cost $100', (I) => {
    I.fillField('#cost', '100.00');
    I.selectOption('serviceQuality', '20% - Good');
    I.fillField('#amountOfPeople', '1');
    I.click('CALCULATE!');
    I.see('$ 20.00', '#result');
    I.dontSee('each', '#result');
});

Scenario('I can count a tip for the big company after a great breakfast cost $284.20', (I) => {
    I.fillField('#cost', '284.20');
    I.selectOption('serviceQuality', '30% - Outstanding');
    I.fillField('#amountOfPeople', '7');
    I.click('CALCULATE!');
    I.see('$ 12.18', '#result');
    I.see('each', '#result');
});

Scenario('I can count a tip by pressing enter', (I) => {
    I.fillField('#cost', '20.00');
    I.selectOption('serviceQuality', '15% - Ok');
    I.fillField('#amountOfPeople', '1');
    I.pressKey('Enter');
    I.see('$ 3.00', '#result');
    I.dontSee('each', '#result');
});

Scenario('I will get $0.00 if use bad input values', (I) => {
    I.fillField('#cost', '55.00test');
    I.selectOption('serviceQuality', '5% - Terrible');
    I.fillField('#amountOfPeople', '1test');
    I.click('CALCULATE!');
    I.see('$ 0.00', '#result');
});