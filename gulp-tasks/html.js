const gulp = require('gulp');
    htmlMin = require('gulp-htmlmin'),
    inject = require('gulp-inject'),
    argv = require('yargs').argv,
    gulpif = require('gulp-if'),
    connect = require('gulp-connect'),
    paths = require('./paths.json');

gulp.task('html', ['styles', 'scripts', 'images'], () => {
    const sources = gulp.src([paths.prod.scripts, paths.prod.styles], {read: false});
    
    return gulp.src(paths.dev.html)
        .pipe(inject(sources, { ignorePath: '/build', addRootSlash: false}))
        .pipe(gulpif(argv.production, htmlMin({collapseWhitespace: true})))
        .pipe(gulp.dest(paths.prod.target))
        .pipe(connect.reload());
});