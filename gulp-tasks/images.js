const gulp = require('gulp'),
    imageMin = require('gulp-imagemin'),
    jpegtran = require('imagemin-jpegtran'),
    paths = require('./paths.json');

gulp.task('images', () => {
    return gulp.src(paths.dev.images)
        .pipe(imageMin({ progressive: true, use: [jpegtran()] }))
        .pipe(gulp.dest(paths.prod.imagesTarget));
});