const gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    argv = require('yargs').argv,
    gulpif = require('gulp-if'),
    concat = require('gulp-concat'),
    sourcemaps = require('gulp-sourcemaps'),
    connect = require('gulp-connect'),
    paths = require('./paths.json');

gulp.task('scripts', () => {
    return gulp.src(paths.dev.scripts)
        .pipe(gulpif(argv.production, sourcemaps.init()))
        .pipe(gulpif(argv.production, concat('app.min.js')))
        .pipe(gulpif(argv.production, uglify()))
        .pipe(gulpif(argv.production, sourcemaps.write()))
        .pipe(gulp.dest(paths.prod.scriptsTarget))
        .pipe(connect.reload());
});