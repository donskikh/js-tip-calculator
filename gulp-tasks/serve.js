const gulp = require('gulp'),
    connect = require('gulp-connect'),
    paths = require('./paths.json');

gulp.task('connect', ['watch'], () => {
  connect.server({
    root: paths.prod.target,
    livereload: true
  });
});

gulp.task('serve', ['build'], () => {
    gulp.start('connect');
});