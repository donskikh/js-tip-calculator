const gulp = require('gulp'),
    postcss = require('gulp-postcss'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    concat = require('gulp-concat'),
    argv = require('yargs').argv,
    gulpif = require('gulp-if'),
    connect = require('gulp-connect'),
    paths = require('./paths.json');

gulp.task('styles', () => {
    return gulp.src(paths.dev.styles)
        .pipe(gulpif(argv.production, sourcemaps.init()))
        .pipe(postcss([ autoprefixer({ browsers: ['last 2 versions'] }) ]))
        .pipe(gulpif(argv.production, concat('styles.min.css')))
        .pipe(gulpif(argv.production, cleanCSS()))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.prod.stylesTarget))
        .pipe(connect.reload());
});