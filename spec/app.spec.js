/*globals TipCalculator */
'use strict';

describe("Tip calculator", function() {
    var resultAmount, pluralFlag, amountOfPeople, cost, serviceQuality, actionCallButton, mainContent;

    function addElementToBody(type, id, className, typeAttribute) {
        var elem = document.createElement(type);
        elem.id = id;

        if (className) {
            elem.className = className;
        }
        if (typeAttribute) {
            elem.setAttribute('type', typeAttribute);
        }
        document.body.appendChild(elem);

        return elem;
    }

    beforeAll(function() {
        resultAmount = addElementToBody('div', 'resultAmount');
        pluralFlag = addElementToBody('div', 'pluralFlag', 'hidden');
        amountOfPeople = addElementToBody('input', 'amountOfPeople', null, 'number');
        cost = addElementToBody('input', 'cost');
        serviceQuality = addElementToBody('input', 'serviceQuality');
        actionCallButton = addElementToBody('input', 'actionCall', null, 'button');
        mainContent = addElementToBody('div', 'content');
    });

    beforeEach(function() {
        resultAmount.innerHTML = "0.00";
        pluralFlag.className = 'hidden';
        amountOfPeople.value = 1;
        cost.value = 0;
        serviceQuality.value = 30;
    });

    afterAll(function() {
        resultAmount.remove();
        pluralFlag.remove();
        amountOfPeople.remove();
        cost.remove();
        serviceQuality.remove();
    });

    describe('interface', function() {

        it('should contain the next public methods', function() {
            expect(TipCalculator.getCost).toBeDefined();
            expect(TipCalculator.getServiceQuality).toBeDefined();
            expect(TipCalculator.getAmountOfPeople).toBeDefined();
            expect(TipCalculator.getResultAmount).toBeDefined();
            expect(TipCalculator.getPluralFlag).toBeDefined();
            expect(TipCalculator.process).toBeDefined();
            expect(TipCalculator.calculateTip).toBeDefined();
            expect(TipCalculator.isHidden).toBeDefined();
            expect(TipCalculator.displayResult).toBeDefined();
            expect(TipCalculator.togglePluralFlag).toBeDefined();
        });
    });

    describe('getAmountOfPeople()', function() {

        it('should return value of people if set', function() {
            expect(TipCalculator.getAmountOfPeople()).toEqual(1);
        });

        it('should return 1 if value isn`t set', function() {
            amountOfPeople.value = null;
            expect(TipCalculator.getAmountOfPeople()).toEqual(1);
        });

        it('should return 1 if the broken or invalid value is set', function() {
            amountOfPeople.value = '123test';
            expect(TipCalculator.getAmountOfPeople()).toEqual(1);
        });

        it('should return 1 if value is less than 0', function() {
            amountOfPeople.value = -5;
            expect(TipCalculator.getAmountOfPeople()).toEqual(1);
        });

        it('should return 1000 if value is greater than 1000', function() {
            amountOfPeople.value = 10000;
            expect(TipCalculator.getAmountOfPeople()).toEqual(1000);
        });

        it('should return 1000 if value is equal to 1000', function() {
            amountOfPeople.value = 1000;
            expect(TipCalculator.getAmountOfPeople()).toEqual(1000);
        });

        it('should return 1 if value is equal to 1', function() {
            amountOfPeople.value = 1;
            expect(TipCalculator.getAmountOfPeople()).toEqual(1);
        });
    });

    describe('getCost()', function() {

        it('should return cost value if set', function() {
            cost.value = 10;
            expect(TipCalculator.getCost()).toEqual(10);
        });

        it('should return 0 cost value if isn`t set', function() {
            cost.value = null;
            expect(TipCalculator.getCost()).toEqual(0);
        });

        it('should return 0 cost value if it`s broken or invalid', function() {
            cost.value = '123test';
            expect(TipCalculator.getCost()).toEqual(0);
        });

        it('should return 0 cost value if it`s less than 0', function() {
            cost.value = -10;
            expect(TipCalculator.getCost()).toEqual(0);
        });

        it('should return 10^6 cost value if it`s greater the than 10^6', function() {
            cost.value = 1000003;
            expect(TipCalculator.getCost()).toEqual(1000000);
        });

        it('should return 10^6 cost value if it`s equal to 10^6', function() {
            cost.value = 1000000;
            expect(TipCalculator.getCost()).toEqual(1000000);
        });

        it('should return 0 cost value if it`s equal to 0', function() {
            cost.value = 0;
            expect(TipCalculator.getCost()).toEqual(0);
        });
    });

    describe('getServiceQuality()', function() {

        it('should return service quality value if set', function() {
            serviceQuality.value = 10;
            expect(TipCalculator.getServiceQuality()).toEqual(10);
        });

        it('should return 30 cost value if isn`t set', function() {
            serviceQuality.value = null;
            expect(TipCalculator.getServiceQuality()).toEqual(30);
        });

        it('should return 30 cost value if it`s broken or invalid', function() {
            serviceQuality.value = '123test';
            expect(TipCalculator.getServiceQuality()).toEqual(30);
        });

        it('should return 30 cost value if it`s greater than 30', function() {
            serviceQuality.value = 32;
            expect(TipCalculator.getServiceQuality()).toEqual(30);
        });

        it('should return 30 cost value if it`s equal to 30', function() {
            serviceQuality.value = 30;
            expect(TipCalculator.getServiceQuality()).toEqual(30);
        });

        it('should return 5 cost value if it`s less than 5', function() {
            serviceQuality.value = 1;
            expect(TipCalculator.getServiceQuality()).toEqual(5);
        });

        it('should return 5 cost value if it`s equal to 5', function() {
            serviceQuality.value = 5;
            expect(TipCalculator.getServiceQuality()).toEqual(5);
        });
    });

    describe('getResultAmount()', function() {
        it('should return #reslutAmount dom element', function() {
            expect(TipCalculator.getResultAmount().id).toEqual('resultAmount');
        });
    });

    describe('getPluralFlag()', function() {
        it('should return #pluralFlag dom element', function() {
            expect(TipCalculator.getPluralFlag().id).toEqual('pluralFlag');
        });
    });

    describe('process()', function() {

        beforeEach(function() {
            spyOn(TipCalculator, 'getAmountOfPeople').and.callThrough();
            spyOn(TipCalculator, 'calculateTip').and.callThrough();
            spyOn(TipCalculator, 'displayResult').and.callThrough();
            spyOn(TipCalculator, 'togglePluralFlag').and.callThrough();
        });

        afterEach(function() {
            TipCalculator.getAmountOfPeople.calls.reset();
            TipCalculator.calculateTip.calls.reset();
            TipCalculator.displayResult.calls.reset();
            TipCalculator.togglePluralFlag.calls.reset();
        });

        it('shuould call getAmountOfPeople() once', function() {
            TipCalculator.process();
            expect(TipCalculator.getAmountOfPeople.calls.count()).toEqual(1);
        });

        it('shuould call calculateTip() once', function() {
            TipCalculator.process();
            expect(TipCalculator.calculateTip.calls.count()).toEqual(1);
        });

        it('shuould call displayResult() once', function() {
            TipCalculator.process();
            expect(TipCalculator.calculateTip.calls.count()).toEqual(1);
        });

        it('shuould call togglePluralFlag() once', function() {
            TipCalculator.process();
            expect(TipCalculator.calculateTip.calls.count()).toEqual(1);
        });
    });

    describe('calculateTip()', function() {

        beforeEach(function() {
            spyOn(TipCalculator, 'getCost').and.callThrough();
            spyOn(TipCalculator, 'getServiceQuality').and.callThrough();
            cost.value = 120;
            serviceQuality.value = 10;
        });

        afterEach(function() {
            TipCalculator.getCost.calls.reset();
            TipCalculator.getServiceQuality.calls.reset();
        });

        it('should return correct value based on cost, service quality and amount of people', function() {
            var amountOfPeople = 2;
            expect(TipCalculator.calculateTip(amountOfPeople)).toEqual('6.00');
        });

        it('should correct value even if amount of people isn`t set', function() {
            expect(TipCalculator.calculateTip()).toEqual('12.00');
        });
    });

    describe('isHidden()', function() {
        var amountOfPeople;
        
        it('should return empty string if amount of people is greater than 1', function() {
            amountOfPeople = 5;
            expect(TipCalculator.isHidden(amountOfPeople)).toEqual('');
        });

        it('should return `hidden` if amount of people equal 1', function() {
            amountOfPeople = 1;
            expect(TipCalculator.isHidden(amountOfPeople)).toEqual('hidden');
        });
    });
    
    describe('displayResult()', function() {
        it('should set given result into the #resultAmount dom element', function() {
            var expectedText = 'test-result';
            TipCalculator.displayResult(expectedText);
            expect(document.getElementById('resultAmount').innerHTML).toEqual(expectedText);
        });
    });
    
    describe('togglePluralFlag()', function() {
        var amountOfPeople;
        
         beforeEach(function() {
            spyOn(TipCalculator, 'isHidden').and.callThrough();
            spyOn(TipCalculator, 'getPluralFlag').and.callThrough();
        });

        afterEach(function() {
            TipCalculator.isHidden.calls.reset();
            TipCalculator.getPluralFlag.calls.reset();
        });
        
        it('should call isHidden() once', function() {
            TipCalculator.togglePluralFlag();
            expect(TipCalculator.isHidden.calls.count()).toEqual(1);
        });
        
        it('should call getPluralFlag() once', function() {
            TipCalculator.togglePluralFlag();
            expect(TipCalculator.getPluralFlag.calls.count()).toEqual(1);
        });
        
        it('shoud remove `hidden` class from #pluralFlag if amount of people is greater than 1', function() {
            amountOfPeople = 5;
            TipCalculator.togglePluralFlag(amountOfPeople);
            
            expect(document.getElementById('pluralFlag').className).toEqual('');
        });
        
        it('should set `hidden` class to #pluralFlag if amount of people is equals to 1', function() {
            amountOfPeople = 1;
            TipCalculator.togglePluralFlag(amountOfPeople);
            
            expect(document.getElementById('pluralFlag').className).toEqual('hidden');
        });
        
        it('should set `hidden` class to #pluralFlag if amount of people isn`t set', function() {
            amountOfPeople = 1;
            TipCalculator.togglePluralFlag(amountOfPeople);
            
            expect(document.getElementById('pluralFlag').className).toEqual('hidden');
        });
    });
});