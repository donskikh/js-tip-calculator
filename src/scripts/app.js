'use strict';

/** @namespace TipCalculator */
var TipCalculator = (function() {
    var actionCallButton = document.getElementById('actionCall'),
        mainContent = document.getElementById('content'),
        amountOfPeopleId = 'amountOfPeople',
        costId = 'cost',
        resultAmountId = 'resultAmount',
        serviceQualityId = 'serviceQuality',
        pluralFlagId = 'pluralFlag',

        ENTER = 13,
        HIDDEN_CLASS = 'hidden',

        minAmountOfPeople = 1,
        maxAmountOfPeople = 1000,
        defaultAmountOfPeople = 1,

        minCost = 0,
        maxCost = 1000000,
        defaultCost = 0,

        minServiceQuality = 5,
        maxServiceQuality = 30,
        defaultServiceQuality = 30;

    /**
     * Initialize the application
     * @memberof TipCalculator
     */
    function init() {
        TipCalculator.attachHandlers();
    }

    /**
     * Check the value is in the gange of constraints or place it in.
     * @memberof TipCalculator
     * @param   {number} min   minimum value
     * @param   {number} max   maximum value
     * @param   {number} value actual value
     * @returns {number} processed value
     */
    function getInRange(min, max, value) {
        var result = value;

        if (value < min) {
            result = min;
        } else if (value > max) {
            result = max;
        }
        return result;
    }

    /**
     * Get amount of people
     * @memberof TipCalculator
     * @returns {number} actual amount
     */
    function getAmountOfPeople() {
        var amountOfPeople = +document.getElementById(amountOfPeopleId).value || defaultAmountOfPeople;

        return getInRange(minAmountOfPeople, maxAmountOfPeople, amountOfPeople);
    }

    /**
     * Get cost
     * @memberof TipCalculator
     * @returns {number} final cost
     */
    function getCost() {
        var cost = +document.getElementById(costId).value || defaultCost;
        return getInRange(minCost, maxCost, cost);
    }

    /**
     * Get service quality
     * @memberof TipCalculator
     * @returns {number} selected quality rate
     */
    function getServiceQuality() {
        var quality = +document.getElementById(serviceQualityId).value || defaultServiceQuality;

        return getInRange(minServiceQuality, maxServiceQuality, quality);
    }

    /**
     * Get result tip amount
     * @memberof TipCalculator
     * @returns Element dom element
     */
    function getResultAmount() {
        return document.getElementById(resultAmountId);
    }

    /**
     * Return plural flag
     * @memberof TipCalculator
     * @returns Element dom element (checkbox)
     */
    function getPluralFlag() {
        return document.getElementById(pluralFlagId);
    }

    /**
     * Calculate tip
     * @memberof TipCalculator
     * @param   {number} amountOfPeople amount of people
     * @returns {string} result tip amount per person
     */
    function calculateTip(amountOfPeople) {
        var cost = TipCalculator.getCost(),
            serviceQuality = TipCalculator.getServiceQuality(),
            peopleCount = amountOfPeople || defaultAmountOfPeople;

        return (cost * (serviceQuality / 100) / peopleCount).toFixed(2);
    }

    /**
     * Display calculated tip at the bottom
     * @memberof TipCalculator
     * @param {string} tipAmount tip amount
     */
    function displayResult(tipAmount) {
        TipCalculator.getResultAmount().innerHTML = tipAmount;
    }

    /**
     * Calculate do we need to hide 'each' word and return appropriate css class
     * @memberof TipCalculator
     * @param   {number} amountOfPeople amount of people
     * @returns {string} css class to apply
     */
    function isHidden(amountOfPeople) {
        return amountOfPeople > 1 ? '' : HIDDEN_CLASS;
    }

    /**
     * Toggle 'each' flag visibility
     * @memberof TipCalculator
     * @param {number} amountOfPeople amount of people
     */
    function togglePluralFlag(amountOfPeople) {
        TipCalculator.getPluralFlag().className = TipCalculator.isHidden(amountOfPeople || 1);
    }

    /**
     * Process all required calculations and apply the result
     * @memberof TipCalculator
     */
    function process() {
        var amountOfPeople = TipCalculator.getAmountOfPeople(),
            result = TipCalculator.calculateTip(amountOfPeople);

        TipCalculator.displayResult(result);
        TipCalculator.togglePluralFlag(amountOfPeople);
    }

    /**
     * Attach event handlers. We can trigger process() with 
     * click on the 'CALCULATE!' button or just by pressing enter
     * @memberof TipCalculator
     * @private
     */
    function attachHandlers() {
        if (actionCallButton) {
            actionCallButton.addEventListener('click', process);
        }
        if (mainContent) {
            mainContent.addEventListener('keypress', function(ev) {
                var key = ev.which || ev.keyCode;
                if (key === ENTER) {
                    process();
                }
            });
        }
    }

    return {
        init: init,
        attachHandlers: attachHandlers,
        getCost: getCost,
        getServiceQuality: getServiceQuality,
        getAmountOfPeople: getAmountOfPeople,
        getResultAmount: getResultAmount,
        getPluralFlag: getPluralFlag,
        process: process,
        calculateTip: calculateTip,
        isHidden: isHidden,
        displayResult: displayResult,
        togglePluralFlag: togglePluralFlag
    };
})();

document.addEventListener("DOMContentLoaded", TipCalculator.init);